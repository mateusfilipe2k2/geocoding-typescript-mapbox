import { Request, Response } from "express";
import axios from 'axios';
import { parse } from '@fast-csv/parse';
import fs, { WriteStream } from "fs";

class AddressController {
  async address(request: Request, response: Response) {

    const saida = fs.createWriteStream('data/out.csv');
    const stream = fs.createReadStream(`data/in.csv`)
    const streamCSV = parse().on(`data`, data => forwardGeocoding(data.join(" "), saida))
    stream.pipe(streamCSV);

    response.status(200).download("data/out.csv")
  }
}


async function forwardGeocoding(address, saida: WriteStream) {
  var ACCESS_TOKEN = 'pk.eyJ1IjoibWF0ZXVzMmsyIiwiYSI6ImNsYmd4ZmV3MzA2ZTkzd2xjMDgzdWR2ejYifQ.RudfKTpz0CtaADWcoei8WA';

  var url = 'https://api.mapbox.com/geocoding/v5/mapbox.places/'
    + encodeURIComponent(address) + '.json?access_token='
    + ACCESS_TOKEN + '&limit=1';

  try {
    const response = await axios.get(url);

    if (response.data.features.length == 0) {
      console.log('Localização não encontrada.');
      return;
    }

    let id = Math.random() * (1000000 - 10000) + 10000
    let longitude = response.data.features[0].center[0]
    let latitude = response.data.features[0].center[1]
    let location = response.data.features[0].place_name

    saida.write(`${id},${longitude},${latitude},${location}\n`);    
  } catch (error) {
    console.log('Falha na API MabBox');
    throw error;
  }

}



export { AddressController };
