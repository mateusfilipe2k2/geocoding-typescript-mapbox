// import * as http from "http";
import express from "express"
import { AddressController } from "./controllers/AddressController"

const app = express()

app.listen(3333, () => console.log("Server is running on port 3333"));

const addressController = new AddressController();

app.get("/address", addressController.address);